#include "sys_render.h"

#include "engine/common/comp_logic.h"
#include "engine/common/comp_render.h"
#include "engine/common/logger.h"

#include <GLES3/gl3.h>
#include <entt/entity/registry.hpp>

CubeRenderSys::CubeRenderSys()
    : shader("./shaders/simple.vsh", "./shaders/simple.fsh")
{
    shader.compile();

    // 3 float - vertix
    float vertices[] = {
        -0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        -0.5f, +0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f, -0.5f, +0.5f,
        +0.5f, -0.5f, +0.5f,
        +0.5f, +0.5f, +0.5f,
        +0.5f, +0.5f, +0.5f,
        -0.5f, +0.5f, +0.5f,
        -0.5f, -0.5f, +0.5f,

        -0.5f, +0.5f, +0.5f,
        -0.5f, +0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, +0.5f,
        -0.5f, +0.5f, +0.5f,

        +0.5f, +0.5f, +0.5f,
        +0.5f, +0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, +0.5f,
        +0.5f, +0.5f, +0.5f,

        -0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, -0.5f,
        +0.5f, -0.5f, +0.5f,
        +0.5f, -0.5f, +0.5f,
        -0.5f, -0.5f, +0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f, +0.5f, -0.5f,
        +0.5f, +0.5f, -0.5f,
        +0.5f, +0.5f, +0.5f,
        +0.5f, +0.5f, +0.5f,
        -0.5f, +0.5f, +0.5f,
        -0.5f, +0.5f, -0.5f
    };

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
}

void CubeRenderSys::update(entt::registry& reg, const ICamera& camera)
{
    glUseProgram(shader.handle());

    Mat4f view = glm::identity<Mat4f>();
    Mat4f projection = glm::identity<Mat4f>();
    //camera.getViews(view, projection);
    shader.setMat4f("projection", projection);
    shader.setMat4f("view", view);

    auto cubesView = reg.view<cl::Transform, cr::Cube>();
    for (auto ent : cubesView) {
        //auto& posCmp = cubesView.get<TransformCmp>(ent);
        //auto& rectCmp = cubesView.get<CubeRendCmp>(ent);

        Mat4f model = glm::identity<Mat4f>();
        //posCmp.trans.getModelMatrix(model);
        shader.setMat4f("model", model);

        // TODO: make border color
        Vec4f col { 0, 0, 0, 0 };
        col.r = 0xff;
        shader.setVec4f("color", col);

        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
    }
}
