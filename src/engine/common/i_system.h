#pragma once

#include <entt/entity/fwd.hpp>

class ICamera;

class ISys {
public:
    bool isEnabled() { return !isPaused; }
    void enable() { isPaused = false; }
    void disable() { isPaused = true; }

private:
    bool isPaused = false;
};

struct ILogicSys : public ISys {
    virtual ~ILogicSys() = default;
    virtual void update(entt::registry& reg, float dt) = 0;
};

struct IRenderSys : public ISys {
    virtual ~IRenderSys() = default;
    virtual void update(entt::registry& reg, const ICamera& camera) = 0;
};
