#include "scene_cube.h"

#include "engine/common/camera_3d.h"
#include "engine/common/comp_logic.h"
#include "engine/common/comp_render.h"
#include "engine/common/logger.h"
#include "engine/common/sys_logic.h"
#include "engine/common/sys_render.h"

CubeScene::CubeScene(GameWindow& window)
    : IScene(window)
{
    camera = new Camera3D(window, Vec3f(0.f, 0.f, 3.f));

    initLogicSystems();
    initRenderSystems();
    initEntities();
}

CubeScene::~CubeScene()
{
    for (ILogicSys* sys : logicSystems) {
        delete sys;
    }
    logicSystems.clear();

    for (IRenderSys* sys : renderSystems) {
        delete sys;
    }
    renderSystems.clear();

    ASSERT(camera);
    delete camera;
    camera = nullptr;
}

void CubeScene::input(float dt, const SDL_Event& event)
{
    if (inputSys) {
        inputSys->onInput(event);
    }
}

void CubeScene::update(float dt)
{
    for (ILogicSys* sys : logicSystems) {
        sys->update(reg, dt);
    }
}

void CubeScene::render(float dt)
{
    ICamera* cam = nullptr;
    for (IRenderSys* sys : renderSystems) {
        sys->update(reg, *cam);
    }
}

void CubeScene::initLogicSystems()
{
    inputSys = new InputSys();
    logicSystems.push_back(inputSys);
    logicSystems.push_back(new MoveSys());
}

void CubeScene::initRenderSystems()
{
    renderSystems.push_back(new CubeRenderSys());
}

void CubeScene::initEntities()
{
    { // cube
        auto entity = reg.create();
        reg.assign<cl::Transform>(entity);
        reg.assign<cr::Cube>(entity);
    }
}
