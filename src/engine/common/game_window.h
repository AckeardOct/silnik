#pragma once

#include "engine/math/vec.h"

class SDL_Renderer;
class SDL_Window;

class IScene;

class GameWindow {
public:
    GameWindow(int argc, char** argv);
    ~GameWindow();

    void loop();
    Vec2f getSize() const;
    Vec2f getCenter() const;

private:
    bool initSDL();
    bool initOpenGL();
    void processInput(float dt);
    void update(float dt);
    void render(float dt);

private:
    IScene* scene = nullptr;
    SDL_Window* sdl_window = nullptr;
    SDL_Renderer* sdl_renderer = nullptr;
    void* gl_context = nullptr;

    bool quitRequested = false;
};
