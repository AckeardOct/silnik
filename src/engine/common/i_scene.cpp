#include "i_scene.h"

#include "engine/common/logger.h"
#include "engine/common/scene_cube.h"

IScene* IScene::Make(StringRef sceneName, GameWindow& window)
{
    if ("CubeScene" == sceneName) {
        return new CubeScene(window);
    } else {
        ASSERT_FAIL("Can't create unknown scene: %s", sceneName);
    }
    return nullptr;
}
