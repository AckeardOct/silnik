#pragma once

#include <SDL2/SDL_events.h>

#include "engine/common/str.h"

class GameWindow;

class IScene {
public:
    IScene(GameWindow& window)
        : window(window)
    {
    }
    virtual ~IScene() = default;

    static IScene* Make(StringRef sceneName, GameWindow& window);

public: // abstract interface
    virtual void input(float dt, const SDL_Event& event)
        = 0;
    virtual void update(float dt) = 0;
    virtual void render(float dt) = 0;

protected:
    GameWindow& window;
};
