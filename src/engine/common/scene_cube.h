#pragma once

#include <entt/entity/registry.hpp>
#include <vector>

#include "engine/common/i_scene.h"

class InputSys;
class ILogicSys;
class IRenderSys;
class ICamera;
class GameWindow;

class CubeScene : public IScene {
public:
    CubeScene(GameWindow& window);
    virtual ~CubeScene() override;

public: // IScene interface
    virtual void input(float dt, const SDL_Event& event) override;
    virtual void update(float dt) override;
    virtual void render(float dt) override;

private:
    void initLogicSystems();
    void initRenderSystems();
    void initEntities();

private:
    entt::registry reg;
    std::vector<ILogicSys*> logicSystems;
    std::vector<IRenderSys*> renderSystems;
    ICamera* camera = nullptr;
    InputSys* inputSys = nullptr;
};
