#include "sys_logic.h"

#include "engine/math/vec.h"

void InputSys::onInput(const SDL_Event& event)
{
    switch (event.type) {
    case SDL_KEYDOWN:
        if (false == keys[event.key.keysym.sym]) {
            keys[event.key.keysym.sym] = true;
            events.push_back(event);
        }
        break;
    case SDL_KEYUP:
        if (true == keys[event.key.keysym.sym]) {
            keys[event.key.keysym.sym] = false;
            events.push_back(event);
        }
        break;
    }
}

void InputSys::update(entt::registry& reg, float dt)
{
    Vec2f direction(0, 0);
    for (auto& iter : keys) {
        if (iter.second) {
            switch (iter.first) {
            case SDLK_UP:
                direction.y -= 1.f;
                break;
            case SDLK_DOWN:
                direction.y += 1.f;
                break;
            case SDLK_LEFT:
                direction.x -= 1.f;
                break;
            case SDLK_RIGHT:
                direction.x += 1.f;
                break;
            }
        }
    }

    if (!m::isZero(direction)) {
        direction = normalize(direction);
        //physUpdate(direction, reg, dt);
        //freeFlyUpdate(direction, reg, dt);
    }

    events.clear(); // TODO: move to SCOPE_EXIT
}
