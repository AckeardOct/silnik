#pragma once

#include "engine/common/i_system.h"
#include "engine/common/shader.h"

class CubeRenderSys : public IRenderSys {
public:
    explicit CubeRenderSys();

public: // IRenderSys interface
    virtual void update(entt::registry& reg, const ICamera& camera) override;

private:
    uint VAO = 0;
    uint VBO = 0;
    Shader shader;
};
