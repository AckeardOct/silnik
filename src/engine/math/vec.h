#pragma once

#include <GLM/gtc/quaternion.hpp>
#include <GLM/mat4x4.hpp>
#include <GLM/vec2.hpp>
#include <GLM/vec3.hpp>
#include <GLM/vec4.hpp>

using Vec2f = glm::vec2;
using Vec3f = glm::vec3;
using Vec4f = glm::vec4;
using Mat4f = glm::mat4;
using Quat = glm::quat;
using Color = Vec4f;

// GLOBALS
const Vec2f Vec2f_Zero(0.f, 0.f);

const Vec3f Vec3f_Zero(0.f, 0.f, 0.f);
const Vec3f Vec3f_UnitX(1.f, 0.f, 0.f);
const Vec3f Vec3f_UnitY(0.f, 1.f, 0.f);
const Vec3f Vec3f_UnitZ(0.f, 0.f, 1.f);

namespace m {
bool isZero(const Vec2f& vec);
}
