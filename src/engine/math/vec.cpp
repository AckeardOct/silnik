#include "vec.h"

namespace m {
const float EPSILON = static_cast<float>(1e-5);
}

bool m::isZero(const Vec2f& vec)
{
    if (std::abs(vec.x) > EPSILON) {
        return false;
    } else if (std::abs(vec.y) > EPSILON) {
        return false;
    }
    return true;
}
