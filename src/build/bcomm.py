#!/usr/bin/env python3

def ABS_PATH(path):
    from os.path import abspath
    return abspath(path)


def JOIN_PATH(left, *right):
    from os.path import join
    return join(left, *right)


def EXISTS(path):
    from os.path import exists
    return exists(path)


def MK_DIRS(path):
    from os import makedirs
    return makedirs(path)


def RM_DIRS(path):
    from shutil import rmtree
    if EXISTS(path):
        rmtree(path)


def RM_FILE(path):
    from os import remove
    if EXISTS(path):
        remove(path)


def EXECUTE(cmd):
    from os import system
    print('========== EXECUTE COMMAND: ')
    print(cmd)
    print('====================')
    return system(cmd)

def RM_FILES_RECUR(path, *masks):
    from glob import glob
    for mask in masks:
        files = glob(path + '/**/' + mask, recursive=False)
        for file in files:
            RM_FILE(file)


def RENAME(src, dst):
    from os import rename
    rename(src, dst)


def MOVE(src, dst):
    from shutil import move
    move(src, dst)