#!/usr/bin/env python

from os.path import join



class Ide:
    def __init__(self, bfs):
        self.bfs = bfs
        self.includes_file   = ".silnik.includes"
        self.files_list_file = ".silnik.files"

    def isFilesExists(self):
        from os.path import exists
        return exists(self.bfs.root(self.includes_file))
    
    def setIncludes(self, includes):
        with open(self.bfs.root(self.includes_file), 'w+') as file:
            for inc in includes:
                file.write(inc + '\n')
    
    def setFiles(self, file_paths):
        with open(self.bfs.root(self.files_list_file), 'w+') as file:
            for fn in file_paths:
                file.write(fn + '\n')
        


class Bfs:
    def __init__(self, root_dir):
        self.root_dir = root_dir        
        self.ide = Ide(self)

    def package(self, path):
        from os.path import basename
        return basename(path)    

    def isIdeExists(self):
        return self.ide.isFilesExists()

    def setIdeIncludes(self, includes):
        return self.ide.setIncludes(includes)
    
    def setIdeFiles(self, files):
        return self.ide.setFiles(files)

    # dirs
    def root(self, *path):
        return join(self.root_dir, *path)
    
    def build(self, *path):
        return join(self.root('build'), *path)
    
    def libs(self, *path):
        return join(self.build('libs'), *path)

    def bin(self, *path):
        return join(self.build('bin'), *path)

    def obj(self, *path):
        return join(self.build('obj'), *path)
    
    def conan(self, *path):
        return join(self.build('conan'), *path)
    
    def src(self, *path):
        return join(self.root('src'), *path)

    def lib3dpart(self, *path):
        return join(self.root('lib3dpart'), *path)
    
    


if __name__ == "__main__":
    BFS = Bfs("/mega-root/")
    print(BFS.root('post'))
    print(BFS.build('post'))
    print(BFS.libs('post'))
    print(BFS.conan('post'))
    print(BFS.src('post'))
    print(BFS.bin('post'))
    print(BFS.package("/root/lol/bin"))
