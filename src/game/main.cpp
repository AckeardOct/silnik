#include "engine/common/game_window.h"

int main (int argc, char** argv)
{
    GameWindow window(argc, argv);
    window.loop();
    return 0;
}
